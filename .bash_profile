# Executed for login shells

# Load the shell dotfiles, and then some:
# * ~/.path can be used to extend `$PATH`.
# * ~/.extra can be used for other settings you don’t want to commit.
for file in ~/.bash/{path,bash_prompt,exports,aliases,functions,extra}; do
  [ -r "$file" ] && source "$file"
done
unset file

# http://code-worrier.com/blog/autocomplete-git
if [ -f ~/.git-completion.bash ]; then
  . ~/.git-completion.bash
fi
