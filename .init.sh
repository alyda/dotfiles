#!/bin/bash

# Ask for the administrator password upfront
sudo -v

# Keep-alive: update existing `sudo` time stamp until this script has finished
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

# Install OSX Command-Line Tools and XQuartz, or update
if ! command -v xcode-select >/dev/null 2>&1; then
  xcode-select --install
else
  sudo softwareupdate -i -a
fi

# ==============================================================================
#  Ruby
# ==============================================================================

# RVM

# install SASS
sudo gem install sass

# Get Homebrew
if brew list -1 | grep -q "^${pkg}\$"; then
    $(which ruby) -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# ==============================================================================
#  Package Manager
# ==============================================================================

# Make sure we're using the latest Homebrew
brew update

# Upgrade any already-installed formulae
brew upgrade --all

# Install GNU core utilities (those that come with OSX are outdated).
# Don't forget to add `$(brew --prefix coreutils)/libexec/gnubin` to `$PATH`
# brew install coreutils
# sudo ln -s /usr/local/bin/gsha256sum /usr/local/bin/sha256sum
# brew install findutils
# brew install bash

# Install more recent versions of some OSX tools
# brew tap homebrew/dupes
# brew install homebrew/dupes/grep

# Install other useful binaries
brew install git node
# brew install sass

# Remove outdated versions from the cellar.
brew cleanup

# ==============================================================================
#  Bash
# ==============================================================================

cd "$HOME"
if [ ! -d ".git" ]; then
  git init
  git remote add origin https://gitlab.com/alyda/dotfiles.git
  git fetch
  git checkout -t origin/master
else
  git fetch
fi

source ~/.bash_profile

# ==============================================================================
#  Applications
# ==============================================================================

apps=(
  "bittorrent-sync"
  "dropbox"
  "1password"
  "google-chrome-canary"
  "slack"
  "sketch"
  "visual-studo-code"
  "sourcetree"
  "kaleidoscope"
  "carbon-copy-cloner"
  "vlc"
  "onyx"
  "minecraft"
)

for app in "${apps[@]}" ; do
  brew cask install $app
done

# https://packagecontrol.io/installation

# https://packagecontrol.io/docs/syncing#dropbox-osx

# ==============================================================================
#  Node
# ==============================================================================

packages=(
# "bower"
  "grunt"
  "grunt-cli"
  "csslint"
  "jslint"
  "jscs"
# "mocha"
# "chai"
)

for package in "${packages[@]}" ; do
  npm install $package -g
done

# ==============================================================================
#  OSX
# ==============================================================================

# General
# -----------------------------------------------

# Use Graphite Appearance
defaults write NSGlobalDomain AppleAquaColorVariant -int 6

# Use dark menu bar & dock
defaults write NSGlobalDomain AppleInterfaceStyle Dark
# Possible values: `Dark`, `Light`

# Use Graphite Highlight Color
defaults write NSGlobalDomain AppleHighlightColor -string "0.847059 0.847059 0.862745"
# red     1.000000 0.733333 0.721569
# orange  1.000000 0.874510 0.701961
# yellow  1.000000 0.937255 0.690196
# green   0.752941 0.964706 0.678431
# blue
# purple  0.968627 0.831373 1.000000
# pink    1.000000 0.749020 0.823529
# brown   0.929412 0.870588 0.792157
# graphite  0.847059 0.847059 0.862745
# --------
# gray 0.780400 0.815700 0.858800

# Set sidebar icon size to medium
defaults write NSGlobalDomain NSTableViewDefaultSizeMode -int 2

# Show scroll bars: When scrolling
defaults write NSGlobalDomain AppleShowScrollBars -string "WhenScrolling"
# Possible values: `WhenScrolling`, `Automatic` and `Always`

# Click in the scroll bar to
#   false: jump to the next page
#   true: jump to the spot that's clicked
defaults write -g AppleScrollerPagingBehavior -bool false

# Default web browser: Google Chrome

# Ask to keep changes when closing documents: NO

# Close windows when quitting an app: YES??

# Recent items: 10 (Documents, Apps, and Servers)

# Allow Handoff between this Mac and your iCloud devices: YES

# Use LCD font smoothing when available
# Enable subpixel font rendering on non-Apple LCDs
defaults write NSGlobalDomain AppleFontSmoothing -int 2

# Desktop & Screen Saver
# -----------------------------------------------

# Desktop: Yosemite

# Icon Size: 64x64
# Increase the size of icons on the desktop and in other icon views
/usr/libexec/PlistBuddy -c "Set :DesktopViewSettings:IconViewSettings:iconSize 64" ~/Library/Preferences/com.apple.finder.plist
/usr/libexec/PlistBuddy -c "Set :FK_StandardViewSettings:IconViewSettings:iconSize 64" ~/Library/Preferences/com.apple.finder.plist
/usr/libexec/PlistBuddy -c "Set :StandardViewSettings:IconViewSettings:iconSize 64" ~/Library/Preferences/com.apple.finder.plist


# Grid Spacing:
# Increase grid spacing for icons on the desktop and in other icon views
# /usr/libexec/PlistBuddy -c "Set :DesktopViewSettings:IconViewSettings:gridSpacing 100" ~/Library/Preferences/com.apple.finder.plist
# /usr/libexec/PlistBuddy -c "Set :FK_StandardViewSettings:IconViewSettings:gridSpacing 100" ~/Library/Preferences/com.apple.finder.plist
# /usr/libexec/PlistBuddy -c "Set :StandardViewSettings:IconViewSettings:gridSpacing 100" ~/Library/Preferences/com.apple.finder.plist


# Text size: 12
# Label position: bottom
# Show item info to the right of the icons on the desktop
# /usr/libexec/PlistBuddy -c "Set DesktopViewSettings:IconViewSettings:labelOnBottom false" ~/Library/Preferences/com.apple.finder.plist


# Show item info: NO
# Show icon preview: YES

# Enable snap-to-grid for icons on the desktop and in other icon views
/usr/libexec/PlistBuddy -c "Set :DesktopViewSettings:IconViewSettings:arrangeBy grid" ~/Library/Preferences/com.apple.finder.plist
/usr/libexec/PlistBuddy -c "Set :FK_StandardViewSettings:IconViewSettings:arrangeBy grid" ~/Library/Preferences/com.apple.finder.plist
/usr/libexec/PlistBuddy -c "Set :StandardViewSettings:IconViewSettings:arrangeBy grid" ~/Library/Preferences/com.apple.finder.plist


# Screen Saver: Flurry, 10 minutes, with clock

# Dock
# -----------------------------------------------

# Set the icon size of Dock items to 36 pixels
defaults write com.apple.dock tilesize -int 36

# Magnification: Max

# Position on screen: Bottom

# Minimize windows using: Genie effect
# Double-click a window's title bar to minimixe: NO
# Minimize windows into application icon: NO

# Animate opening applications from the Dock
defaults write com.apple.dock launchanim -bool true

# Automatically hide and show the Dock
defaults write com.apple.dock autohide -bool true

# Show indicator lights for open applications in the Dock
defaults write com.apple.dock show-process-indicators -bool true

# APPLICATIONS
defaults delete com.apple.dock persistent-apps
defaults delete com.apple.dock persistent-others

# Slack, Canary, Sketch, VS Code, SourceTree
defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Users/$USER/Applications/Slack.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Users/$USER/Applications/Google Chrome Canary.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Users/$USER/Applications/Sketch.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Users/$USER/Applications/Visual Studio Code.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
defaults write com.apple.dock persistent-apps -array-add "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>/Users/$USER/Applications/SourceTree.app</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"

# Mission Control
# -----------------------------------------------

# Don’t automatically rearrange Spaces based on most recent use
defaults write com.apple.dock mru-spaces -bool false

# When switching to an application, switch to a Space with open windows for the application: YES
# Group windows by application: YES
# Displays have separate spaces: YES

# Disable the dashboard completely
defaults write com.apple.dashboard mcx-disabled -boolean YES;

# Keyboard and Mouse Shortcuts
# Mission Control:      ^ up
# Application windows:  ^ down
# Show Desktop:         F11
# Show Dashboard:       F12

# Hot corners
# Possible values:
#  0: no-op
#  2: Mission Control
#  3: Show application windows
#  4: Desktop
#  5: Start screen saver
#  6: Disable screen saver
#  7: Dashboard
# 10: Put display to sleep
# 11: Launchpad
# 12: Notification Center

# Top left screen corner → Mission Control
# defaults write com.apple.dock wvous-tl-corner -int 2
# defaults write com.apple.dock wvous-tl-modifier -int 0

# Top right screen corner → Desktop
# defaults write com.apple.dock wvous-tr-corner -int 4
# defaults write com.apple.dock wvous-tr-modifier -int 0

# Bottom right screen corner → Start screen saver
defaults write com.apple.dock wvous-br-corner -int 5
defaults write com.apple.dock wvous-br-modifier -int 0

# Language & Region
# -----------------------------------------------

# Security & Privacy
# -----------------------------------------------

# Require password 1 minute after sleep or screen saver begins
defaults write com.apple.screensaver askForPassword -int 1
defaults write com.apple.screensaver askForPasswordDelay -int 60

# Show a message when the screen is locked: YES
# I am lost, please return me to $USER +1 XXX XXX XXXX or email

# Spotlight
# -----------------------------------------------

# Hide Spotlight tray-icon (and subsequent helper)
# sudo chmod 600 /System/Library/CoreServices/Search.bundle/Contents/MacOS/Search
# Disable Spotlight indexing for any volume that gets mounted and has not yet
# been indexed before.
# Use `sudo mdutil -i off "/Volumes/foo"` to stop indexing any volume.
# sudo defaults write /.Spotlight-V100/VolumeConfiguration Exclusions -array "/Volumes"
# Change indexing order and disable some file types
defaults write com.apple.spotlight orderedItems -array \
  '{"enabled" = 1;"name" = "APPLICATIONS";}' \
  '{"enabled" = 1;"name" = "MENU_SPOTLIGHT_SUGGESTIONS";}' \
  '{"enabled" = 1;"name" = "MENU_CONVERSION";}' \
  '{"enabled" = 1;"name" = "MENU_EXPRESSION";}' \
  '{"enabled" = 1;"name" = "MENU_DEFINITION";}' \
  '{"enabled" = 1;"name" = "SYSTEM_PREFS";}' \
  '{"enabled" = 1;"name" = "DOCUMENTS";}' \
  '{"enabled" = 1;"name" = "DIRECTORIES";}' \
  '{"enabled" = 0;"name" = "PRESENTATIONS";}' \
  '{"enabled" = 1;"name" = "SPREADSHEETS";}' \
  '{"enabled" = 1;"name" = "PDF";}' \
  '{"enabled" = 1;"name" = "MESSAGES";}' \
  '{"enabled" = 1;"name" = "CONTACT";}' \
  '{"enabled" = 1;"name" = "EVENT_TODO";}' \
  '{"enabled" = 1;"name" = "IMAGES";}' \
  '{"enabled" = 1;"name" = "BOOKMARKS";}' \
  '{"enabled" = 0;"name" = "MUSIC";}' \
  '{"enabled" = 0;"name" = "MOVIES";}' \
  '{"enabled" = 0;"name" = "FONTS";}' \
  '{"enabled" = 1;"name" = "MENU_OTHER";}' \
  '{"enabled" = 1;"name" = "MENU_WEBSEARCH";}'
# Load new settings before rebuilding the index
# killall mds
# Make sure indexing is enabled for the main volume
# sudo mdutil -i on /
# Rebuild the index from scratch
# sudo mdutil -E /

# Notifications
# -----------------------------------------------

# Displays
# -----------------------------------------------

# Enable HiDPI display modes (requires restart)
sudo defaults write /Library/Preferences/com.apple.windowserver DisplayResolutionEnabled -bool true

# Resolution: Scaled: More Space

# Energy Saver
# -----------------------------------------------

# Battery
# Turn off display after: 3 min
# Put hard disks to sleep when possible: YES
# Slightly dim the display while on battery power: YES
# Enable Power Nap while on battery power: YES

# Power Adapter
# Turn off display after: 15 min
# Prevent computer from sleeping automatically when the display is off: YES
# Put hard disks to sleep when possible: YES
# Wake for Wi-Fi network access: YES
# Enable Power Nap while plugged into a power adapter: YES

# Schedule
# Start up or wake: Weekdays at 9AM
# Sleep: Weekdays at 9PM

# Show battery status in menu bar: YES

# Keyboard
# -----------------------------------------------

# Automatically illuminate built-in MacBook keyboard in low light
defaults write com.apple.BezelServices kDim -bool true
# Turn off keyboard illumination when computer is not used for 5 minutes
defaults write com.apple.BezelServices kDimTime -int 300

# Mouse
# -----------------------------------------------

# Set scroll direction
# defaults write NSGlobalDomain com.apple.swipescrolldirection -bool false

# Secondary click:
# Possible values: OneButton, TwoButton, TwoButtonSwapped
# defaults write com.apple.driver.AppleBluetoothMultitouch.mouse MouseButtonMode -string TwoButton

# Smart zoom enabled, double-tap with one finger (set to 0 to disable)
# defaults write com.apple.driver.AppleBluetoothMultitouch.mouse MouseOneFingerDoubleTapGesture -int 1

# Double-tap with two fingers to Mission Control (set to 0 to disable)
# defaults write com.apple.driver.AppleBluetoothMultitouch.mouse MouseTwoFingerDoubleTapGesture -int 3

# Two finger horizontal swipe
# 0 = Swipe between pages with one finger
# 1 = Swipe between pages
# 2 = Swipe between full screen apps with two fingers, swipe between pages with one finger (Default Mode)
# defaults write com.apple.driver.AppleBluetoothMultitouch.mouse MouseTwoFingerHorizSwipeGesture -int 2

# defaults write com.apple.driver.AppleBluetoothMultitouch.mouse MouseVerticalScroll -int 1
# defaults write com.apple.driver.AppleBluetoothMultitouch.mouse MouseMomentumScroll -int 1
# defaults write com.apple.driver.AppleBluetoothMultitouch.mouse MouseHorizontalScroll -int 1

# Trackpad
# -----------------------------------------------

 Tap to click
# defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true

# Tap with two fingers to emulate right click
# defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadRightClick -bool true

# Enable three finger tap (look up)
# defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadThreeFingerTapGesture -int 2

# Disable three finger drag
# defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadThreeFingerDrag -bool false

# Zoom in or out
# defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadPinch -bool true

# Smart zoom, double-tap with two fingers
# defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadTwoFingerDoubleTapGesture -bool true

# Rotate
# defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadRotate -bool true

# Notification Center
# defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadTwoFingerFromRightEdgeSwipeGesture -int 3

# Swipe between pages with two fingers
# defaults write NSGlobalDomain AppleEnableSwipeNavigateWithScrolls -bool true

# Swipe between full-screen apps
# defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadThreeFingerHorizSwipeGesture -int 2

# Enable other multi-finger gestures
# defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadThreeFingerVertSwipeGesture -int 2
# defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadFourFingerVertSwipeGesture -int 2
# defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadFourFingerPinchGesture -int 2
# defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadFourFingerHorizSwipeGesture -int 2
# defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadFiveFingerPinchGesture -int 2

# defaults write com.apple.dock showMissionControlGestureEnabled -bool true
# defaults write com.apple.dock showAppExposeGestureEnabled -bool true
# defaults write com.apple.dock showDesktopGestureEnabled -bool true
# defaults write com.apple.dock showLaunchpadGestureEnabled -bool true

# Trackpad: enable tap to click for this user and for the login screen
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad Clicking -bool true
defaults -currentHost write NSGlobalDomain com.apple.mouse.tapBehavior -int 1
defaults write NSGlobalDomain com.apple.mouse.tapBehavior -int 1

# Trackpad: map bottom right corner to right-click
# defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadCornerSecondaryClick -int 2
# defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadRightClick -bool true
# defaults -currentHost write NSGlobalDomain com.apple.trackpad.trackpadCornerClickBehavior -int 1
# defaults -currentHost write NSGlobalDomain com.apple.trackpad.enableSecondaryClick -bool true

# Trackpad: swipe between pages with three fingers
defaults write NSGlobalDomain AppleEnableSwipeNavigateWithScrolls -bool true
defaults -currentHost write NSGlobalDomain com.apple.trackpad.threeFingerHorizSwipeGesture -int 2
defaults write com.apple.driver.AppleBluetoothMultitouch.trackpad TrackpadThreeFingerHorizSwipeGesture -int 2

# Trackpad: disable launchpad pinch with thumb and three fingers
# defaults write com.apple.dock showLaunchpadGestureEnabled -int 0

# Disable “natural” (Lion-style) scrolling
defaults write NSGlobalDomain com.apple.swipescrolldirection -bool false

# Printers & Scanners
# -----------------------------------------------

# Sound
# -----------------------------------------------

# iCloud
# -----------------------------------------------

# Internet Accounts
# -----------------------------------------------

# Extensions
# -----------------------------------------------

# Network
# -----------------------------------------------

# Bluetooth
# -----------------------------------------------

# Sharing
# -----------------------------------------------

# Set computer name (as done via System Preferences → Sharing)
# sudo scutil --set ComputerName "MacBook Pro"
# sudo scutil --set HostName "MacBook Pro"
# sudo scutil --set LocalHostName "MacBook-Pro"
# sudo defaults write /Library/Preferences/SystemConfiguration/com.apple.smb.server NetBIOSName -string "MacBook-Pro"

# Users & Groups
# -----------------------------------------------

# Parental Controls
# -----------------------------------------------

# App Store
# -----------------------------------------------

# Dictation & Speech
# -----------------------------------------------

# Date & Time
# -----------------------------------------------

# Startup Disk
# -----------------------------------------------

# Time Machine
# -----------------------------------------------

# enable Time Machine
sudo tmutil enable

# Add Exclusions from local txt file
# cat ./init/exclusions.txt | sed 's/~/\/Users\/'$USER/ | xargs sudo tmutil addexclusion

# Verify Exclusions (if necessary)
# sudo tmutil isexcluded ...

# Save some space
# sudo tmutil disablelocal

# Start backup
# sudo tmutil startbackup

# Accessibility
# -----------------------------------------------

# Flash Player
# -----------------------------------------------

# Java
# -----------------------------------------------

# TERMINAL
# -----------------------------------------------

# Enable “focus follows mouse” for Terminal.app and all X11 apps
# i.e. hover over a window and start typing in it without clicking first
defaults write com.apple.terminal FocusFollowsMouse -string true

# Use a modified version of the Solarized Dark theme by default in Terminal.app 

# open the custom theme so it gets added to the list 
open "$HOME/.settings/Solarized Dark Alyda.terminal"
# wait for 1 second to make sure custom theme was added
sleep 5
# set custom theme
defaults write com.apple.Terminal "Default Window Settings" -string "Solarized Dark Alyda"
defaults write com.apple.Terminal "Startup Window Settings" -string "Solarized Dark Alyda"

# Only use UTF-8 in Terminal.app
defaults write com.apple.terminal StringEncodings -array 4

# [hjuutilainen][4]
CFPreferencesAppSynchronize "com.apple.Terminal"


# Finder
# -----------------------------------------------

echo "Setting Finder preferences"

# GENERAL
# Show these items on the desktop:
# Hard disks: YES
defaults write com.apple.finder ShowHardDrivesOnDesktop -bool true
# External disks: YES
defaults write com.apple.finder ShowExternalHardDrivesOnDesktop -bool true
# CDs, DVDs, and iPods: NO
defaults write com.apple.finder ShowRemovableMediaOnDesktop -bool false
# Connected servers: NO
defaults write com.apple.finder ShowMountedServersOnDesktop -bool false
# New Finder winows show: Home
defaults write com.apple.finder NewWindowTarget -string "PfHm"
# Open folders in tabs instead of new windows: YES

# TAGS

# SIDEBAR
# Show these items in the sidebar:
# Favorites

# Shared

# Devices

# Tags

# ADVANCED

# Finder: show all filename extensions
defaults write NSGlobalDomain AppleShowAllExtensions -bool false

# Disable the warning when changing a file extension
# defaults write com.apple.finder FXEnableExtensionChangeWarning -bool false

# Disable the warning before emptying the Trash
# defaults write com.apple.finder WarnOnEmptyTrash -bool false

# Empty Trash securely by default
# defaults write com.apple.finder EmptyTrashSecurely -bool true

# When performing a search, search the current folder by default
defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"

# ---

# Use list view in all Finder windows by default
# Four-letter codes for the other view modes: `icnv`, `clmv`, `Flwv`
defaults write com.apple.finder FXPreferredViewStyle -string "Nlsv"

# Finder: show status bar
defaults write com.apple.finder ShowStatusBar -bool true

# Finder: show path bar
defaults write com.apple.finder ShowPathbar -bool true

# Finder: allow text selection in Quick Look
defaults write com.apple.finder QLEnableTextSelection -bool true

# Display full POSIX path as Finder window title
defaults write com.apple.finder _FXShowPosixPathInTitle -bool true

# Expand save panel by default
defaults write NSGlobalDomain NSNavPanelExpandedStateForSaveMode -bool true

# ==============================================================================
#  Kill affected applications
# ==============================================================================

for app in "Dock" "Terminal"; do
    killall "$app" > /dev/null 2>&1
done

echo "Done. You may want to restart for all changes to take effect."
# sudo shutdown -r now "Restarting for preferences to take effect"