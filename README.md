:bangbang: **WARNING: YOU ASSUME ALL RISKS ASSOCIATED WITH FOLLOWING ANY OF THE STEPS BELOW** :bangbang:
===

These settings are for my personal use and probably do not suit your needs. Feel free to fork and modify but **DO NOT** run the code below without understanding the changes that will be made to your computer, as I will not be held responsible. There are plenty of comments, so please READ THEM.

---

Getting Started
===============

```shell
$ bash <(curl -sS https://gitlab.com/alyda/dotfiles/raw/master/.init.sh)
```

What does it do?
----------------
1. Install OSX Command-Line Tools, or update
1. Install [SASS][scss]
1. Install [Homebrew][brew]
    + git
    + [node][nodejs]
1. Bash Profile
    + aliases
    + prompt
    + exports
    + functions
    + path
1. Homebrew installs Applications:
    + Dropbox (for syncing 1Password)
    + 1Password
    + Canary (Chrome)
    + Slack
    + VSCode
    + SourceTree
    + Sketch
    + VLC
    + Onyx
    + Minecraft
1. [NPM][npm] installs:
    + grunt & grunt-cli
    + bower
1. Set [OSX Preferences][defaults] :warning:
    + System Preferences :wrench:
    + Dock Applications

Acknowledgements
----------------
I did **not** write most of what you'll find here. It has been sourced from [many others][dotfiles]. I apologize if I have not given someone proper attribution, please feel free to open an issue and I'll give credit where it is due.

---
[scss]: http://sass-lang.com
[brew]: http://brew.sh
[nodejs]: https://nodejs.org
[npm]: https://www.npmjs.com
[defaults]: http://www.defaults-write.com
[dotfiles]: https://dotfiles.github.io